package fabianbrunner.workflowtime;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

public class BilanzActivity extends AppCompatActivity {

    private Button btnFeatures;
    private Button btnMain;
    private Button btnReload;
    private TextView txtTodayHave;
    private TextView txtTodayShould;
    private TextView txtTodayBilanz;
    private TextView txtWeekHave;
    private TextView txtWeekShould;
    private TextView txtWeekBilanz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bilanz);

        txtTodayHave = findViewById(R.id.txtTodayHave_bilanz);
        txtTodayShould = findViewById(R.id.txtTodayShould_bilanz);
        txtTodayBilanz = findViewById(R.id.txtTodayBilanz_bilanz);
        txtWeekHave = findViewById(R.id.txtWeekHave_bilanz);
        txtWeekShould = findViewById(R.id.txtWeekShould_bilanz);
        txtWeekBilanz = findViewById(R.id.txtWeekBilanz_bilanz);

       calculate();

        btnMain = findViewById(R.id.btnMain_bilanz);
        btnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivity();
            }
        });

        btnFeatures = findViewById(R.id.btnFeatures_bilanz);
        btnFeatures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeaturesActivity();
            }
        });

    }

    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    public void openFeaturesActivity() {
        Intent intent = new Intent(this, FeaturesActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    public void calculate() {

        Integer dateDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Integer dateMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        Integer dateYear = Calendar.getInstance().get(Calendar.YEAR);
        String dateFile = dateYear.toString() + dateMonth + dateDay;
        String weekFile = String.valueOf(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));

        float minutesToday = Float.parseFloat(readData("minutes_" + dateFile));
        float minutesWeek = Float.parseFloat(readData("week_" + weekFile));
        float hoursAWeek = Float.parseFloat((readData("hoursAweek")));
        float daysAWeek = Float.parseFloat((readData("daysAweek")));

        float todayHave = roundFloat(minutesToday / 60);
        txtTodayHave.setText(todayHave + " Stunden");

        float todayShould = roundFloat(hoursAWeek / daysAWeek);
        txtTodayShould.setText(todayShould + " Stunden");

        float todayBilanz = roundFloat(todayHave - todayShould);
        txtTodayBilanz.setText(todayBilanz + " Stunden");

        float weekHave = roundFloat(minutesWeek / 60);
        txtWeekHave.setText(weekHave + " Stunden");

        float weekShould = roundFloat(hoursAWeek);
        txtWeekShould.setText(weekShould + " Stunden");

        float weekBilanz = roundFloat(weekHave - weekShould);
        txtWeekBilanz.setText(weekBilanz + " Stunden");

    }

    public String readData(String filename) {
        try {
            FileInputStream inputStream = openFileInput(filename);
            int c;
            String temp = "";
            while ((c = inputStream.read()) != -1) {
                temp = temp + Character.toString((char) c);
            }
            inputStream.close();
            return temp;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public float roundFloat(float number) {
        float res = (float) (((int) (number * 10)) / 10.0);
        return res;
    }
}