package fabianbrunner.workflowtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DatenschutzActivity extends AppCompatActivity {
    private Button btnBack;
    private TextView txtDatenschutz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datenschutz);

        txtDatenschutz = findViewById(R.id.txtDatenschutz_datenschutz);
        txtDatenschutz.setText("Alle Ihre Datein werden lokal gespeichert, also auf dem Gerät oder einem externen Speichermedium, " +
                "je nach Speicherort der Applikation. Die Dateien werden nicht verschlüsselt. Sie sind jedoch versteckt und können " +
                "nicht ohne Weiteres gefunden werden. Jegliche Buchstaben sind in >Bytes< umgewandelt. Dies hat zur Folge, dass sie nicht ohne weiteres gelesen werden " +
                "können. Für einen optimalen Datenschutz empfehlen wir, dass Gerät, beziehungsweise das externe Speichermedium, zu verschlüsseln.") ;

        btnBack = findViewById(R.id.btnBack_datenschutz);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeaturesActivity();
            }
        });
    }

    public void openFeaturesActivity() {
        Intent intent = new Intent(this, FeaturesActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
    }
}
