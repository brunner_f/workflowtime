package fabianbrunner.workflowtime;

import android.content.Intent;
import android.os.FileObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FeaturesActivity extends AppCompatActivity {

    private Button btnMain;
    private Button btnBilanzen;
    private Button btnProfil;
    private Button btnExportAsTxt;;
    private Button btnKranktage;
    private Button btnDatenschutz;
    private Button btnRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_features);

        btnMain = findViewById(R.id.btnMain_features);
        btnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivity();
            }
        });

        btnBilanzen = findViewById(R.id.btnBilanz_features);
        btnBilanzen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBilanzActivity();
            }
        });

        btnProfil = findViewById(R.id.btnProfil);
        btnProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openProfilActivity();
            }
        });

        btnExportAsTxt = findViewById(R.id.btnExportAsTxt);
        btnExportAsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openExportAsTxtActivity();
            }
        });

        btnKranktage = findViewById(R.id.btnKranktage);
        btnKranktage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openKranktageActivity();
            }
        });

        btnDatenschutz = findViewById(R.id.btnDatenschutz);
        btnDatenschutz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatenschutzActivity();
            }
        });

        btnRemove = findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRemoveActivity();
            }
        });
    }

    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    public void openBilanzActivity() {
        Intent intent = new Intent(this, BilanzActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    public void  openProfilActivity() {
        Intent intent = new Intent(this, ProfilActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openExportAsTxtActivity() {
        Intent intent = new Intent(this, ExportAsTxtActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openKranktageActivity() {
        Intent intent = new Intent(this, KranktageActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openDatenschutzActivity() {
        Intent intent = new Intent(this, DatenschutzActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
    }

    public void  openRemoveActivity() {
        Intent intent = new Intent(this, RemoveActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
    }
}