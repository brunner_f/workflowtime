package fabianbrunner.workflowtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class KranktageActivity extends AppCompatActivity {

    private Button btnBack;
    private Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kranktage);

        btnAdd = findViewById(R.id.btnAdd_kranktage);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTag();
            }
        });

        btnBack = findViewById(R.id.btnBack_kranktage);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeaturesActivity();
            }
        });
    }
    public void openFeaturesActivity() {
        Intent intent = new Intent(this, FeaturesActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void addTag() {
        Integer dateDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Integer dateMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        Integer dateYear = Calendar.getInstance().get(Calendar.YEAR);
        String dateFile = dateYear.toString() + dateMonth + dateDay;
        String weekFile = String.valueOf(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));

        Integer timeWeek = Integer.valueOf(readData("week_" + weekFile));
        Integer minutesAday = Integer.valueOf(readData("hoursAweek")) / Integer.valueOf(readData("daysAweek")) * 60;
        Integer timeWeekCal = timeWeek + minutesAday;

        writeData("minutes_" + dateFile, minutesAday.toString());
        writeData("week_" + weekFile, timeWeekCal.toString());

    }

    public void writeData(String filename, String outputString) {
        try {
            FileOutputStream outputStream = openFileOutput(filename, MODE_PRIVATE);
            outputStream.write(outputString.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public String readData(String filename) {
        try {
            FileInputStream inputStream = openFileInput(filename);
            int c;
            String temp = "";
            while ((c = inputStream.read()) != -1) {
                temp = temp + Character.toString((char) c);
            }
            inputStream.close();
            return temp;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
