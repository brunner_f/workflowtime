package fabianbrunner.workflowtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import junit.framework.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private Button btnFeatures;
    private Button btnBilanz;
    private Button btnTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Integer time = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60 + Calendar.getInstance().get(Calendar.MINUTE);
        Integer dateDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Integer dateMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        Integer dateYear = Calendar.getInstance().get(Calendar.YEAR);
        String date = dateDay + "." + dateMonth + "." + dateYear;
        String dateFile = dateYear.toString() + dateMonth + dateDay;
        String weekFile = String.valueOf(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));

        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {
            writeData("lastDate", dateFile);
            writeData("lastWeek", weekFile);
            writeData("startTime", String.valueOf(time * -1));
            writeData("type", "Mit der Arbeit beginnen");
            writeData("protokoll", "");
            writeData("hoursAweek", "40");
            writeData("daysAweek", "5");
            writeData("minutes_" + dateFile, "0");
            writeData("week_" + weekFile, "0");
            startActivity(new Intent(this, ProfilActivity.class));
        }
        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("isFirstRun", false).commit();

        setMinutes(dateFile);
        setWeek(weekFile);

        btnTimer = findViewById(R.id.btnStart);
        btnTimer.setText(readData("type"));
        btnTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Timer();
            }
        });

        btnFeatures = findViewById(R.id.btnFeatures_main);
        btnFeatures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openFeaturesActivity();
            }
        });

        btnBilanz = findViewById(R.id.btnBilanz_main);
        btnBilanz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBilanzActivity();
            }
        });
    }


    public void openFeaturesActivity() {
        Intent intent = new Intent(this, FeaturesActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    public  void openBilanzActivity() {
        Intent intent = new Intent(this, BilanzActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    public void Timer() {

        Integer time = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60 + Calendar.getInstance().get(Calendar.MINUTE);
        Integer dateDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Integer dateMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        Integer dateYear = Calendar.getInstance().get(Calendar.YEAR);
        String date = dateDay + "." + dateMonth + "." + dateYear;
        String dateFile = dateYear.toString() + dateMonth + dateDay;
        String weekFile = String.valueOf(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));

        String protokollBisJetzt = readData("protokoll");
        Integer startTime = Integer.valueOf(readData("startTime"));
        Integer timeToday = Integer.valueOf(readData("minutes_" + dateFile));
        Integer timeWeek = Integer.valueOf(readData("week_" + weekFile));
        String minutesNow = String.valueOf(Calendar.getInstance().get(Calendar.MINUTE));

        if(Integer.valueOf(minutesNow) < 10){
            minutesNow = "0" + minutesNow.toString();
        }


        if(btnTimer.getText().length() == 24) {
            //Wenn der Timer gestoppt wird
            btnTimer.setText("Mit der Arbeit beginnen");
            writeData("type", "Mit der Arbeit beginnen");
            Integer workedTime = time + startTime + timeToday;
            writeData("minutes_" + dateFile, workedTime.toString());
            Integer workedTimeWeek = time + startTime + timeWeek;
            writeData("week_" + weekFile, workedTimeWeek.toString());
            writeData("protokoll",
                     protokollBisJetzt + "\n" + date  + "    " + Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + ":" + minutesNow + "    AUFGEHOERT ZU ARBEITEN");
            return;
        }

        else {
            //Wenn der Timer gestartet wird
            btnTimer.setText("Mit der Arbeit aufhoeren");
            writeData("type", "Mit der Arbeit aufhoeren");
            writeData("startTime", String.valueOf(0-time));
            writeData("protokoll",
                    protokollBisJetzt + "\n" + date  + "    " + Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + ":" + minutesNow + "    BEGONNEN ZU ARBEITEN");

            return;
        }

    }

    public void writeData(String filename, String outputString) {
        try {
            FileOutputStream outputStream = openFileOutput(filename, MODE_PRIVATE);
            outputStream.write(outputString.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public String readData(String filename) {
        try {
            FileInputStream inputStream = openFileInput(filename);
            int c;
            String temp = "";
            while ((c = inputStream.read()) != -1) {
                temp = temp + Character.toString((char) c);
            }
            inputStream.close();
            return temp;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void setMinutes(String dateFile){
        Integer dateInFile = Integer.valueOf(readData("lastDate"));
        Integer dateNow = Integer.valueOf(dateFile);
        if(dateNow > dateInFile) {
            String protokollBisJetzt = readData("protokoll");
            writeData("minutes_" + dateFile, "0");
            writeData("lastDate", dateFile);
            Integer timeTodayRaw = Integer.valueOf(readData("minutes_" + dateFile));
            float timeToday = roundFloat(timeTodayRaw);
            writeData("protokoll",
                    protokollBisJetzt + "\n" + "TOTAL GEARBEITET HEUTE:    " + timeToday);
        }
        return;
    }

    public  void setWeek(String weekFile) {
        Integer weekInFile = Integer.valueOf(readData("lastWeek"));
        Integer weekNow = Integer.valueOf(weekFile);
        if(weekNow > weekInFile) {
            writeData("week_" + weekFile, "0");
            writeData("lastWeek", weekFile);
        }
        return;
    }

    public float roundFloat(float number) {
        float res = (float) (((int) (number * 10)) / 10.0);
        return res;
    }

}
