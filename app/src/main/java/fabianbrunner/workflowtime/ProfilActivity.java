package fabianbrunner.workflowtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ProfilActivity extends AppCompatActivity {
    private Button btnOk;
    private EditText etHAW;
    private EditText etDAW;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        etHAW = findViewById(R.id.etHAW);
        etHAW.setText(readData("hoursAweek"));
        etDAW = findViewById(R.id.etDAW);
        etDAW.setText(readData("daysAweek"));

        btnOk = findViewById(R.id.btnOk_profil);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etHAW.getText().length() >0) {
                    if(etDAW.getText().length() >0){
                        if(Integer.parseInt(etDAW.getText().toString()) > 0){
                            if(Integer.parseInt(etHAW.getText().toString()) > 0) {
                                if(Integer.parseInt(etDAW.getText().toString()) < 8){
                                    if(Integer.parseInt(etHAW.getText().toString()) < 169){
                                        writeData("hoursAweek", etHAW.getText().toString());
                                        writeData("daysAweek", etDAW.getText().toString());
                                        openFeaturesActivity();
                                    }
                                }
                            }
                        }

                    }
                }
            }
        });

        btnBack = findViewById(R.id.btnBack_profil);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeaturesActivity();
            }
        });
    }


    public void openFeaturesActivity() {
        Intent intent = new Intent(this, FeaturesActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void writeData(String filename, String outputString) {
        try {
            FileOutputStream outputStream = openFileOutput(filename, MODE_PRIVATE);
            outputStream.write(outputString.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public String readData(String filename) {
        try {
            FileInputStream inputStream = openFileInput(filename);
            int c;
            String temp = "";
            while ((c = inputStream.read()) != -1) {
                temp = temp + Character.toString((char) c);
            }
            inputStream.close();
            return temp;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
