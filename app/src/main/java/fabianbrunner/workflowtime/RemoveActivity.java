package fabianbrunner.workflowtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class RemoveActivity extends AppCompatActivity {
    public Button btnBack;
    public Button btnDelete;
    public CheckBox ckbRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove);

        btnBack = findViewById(R.id.btnBack_remove);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeaturesActivity();
            }
        });

        btnDelete = findViewById(R.id.btnDelete_remove);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ckbRemove = findViewById(R.id.ckbRemove);
                if(ckbRemove.isChecked()) {
                    removeAll();
                }
                else {
                    Toast.makeText(RemoveActivity.this, "Sie müssen bestätigen, dass Sie alles löschen wollen!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void openFeaturesActivity() {
        Intent intent = new Intent(this, FeaturesActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
    }

    public void removeAll() {
        Integer time = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60 + Calendar.getInstance().get(Calendar.MINUTE);
        Integer dateDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Integer dateMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        Integer dateYear = Calendar.getInstance().get(Calendar.YEAR);
        String date = dateDay + "." + dateMonth + "." + dateYear;
        String dateFile = dateYear.toString() + dateMonth + dateDay;
        String weekFile = String.valueOf(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));

        writeData("lastDate", dateFile);
        writeData("lastWeek", weekFile);
        writeData("startTime", String.valueOf(time * -1));
        writeData("type", "Mit der Arbeit beginnen");
        writeData("protokoll", "");
        writeData("hoursAweek", "40");
        writeData("daysAweek", "5");
        writeData("minutes_" + dateFile, "0");
        writeData("week_" + weekFile, "0");
        startActivity(new Intent(this, MainActivity.class));

    }

    public void writeData(String filename, String outputString) {
        try {
            FileOutputStream outputStream = openFileOutput(filename, MODE_PRIVATE);
            outputStream.write(outputString.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public String readData(String filename) {
        try {
            FileInputStream inputStream = openFileInput(filename);
            int c;
            String temp = "";
            while ((c = inputStream.read()) != -1) {
                temp = temp + Character.toString((char) c);
            }
            inputStream.close();
            return temp;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
